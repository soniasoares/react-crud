import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Header from './components/Header/Header';
import AddItem from './components/Items/AddItem';
import EditItem from './components/Items/EditItem';
import ItemsList from './components/Items/ItemsList';

import useLocalStorage from './hooks/useLocalStorage';
import ItemsContext from './context/ItemsContext';

const App = () => {
  const [items, setItems] = useLocalStorage('items', []);

  return (
    <BrowserRouter>
      <Header />
      <main role="main" className="main-content">
        <ItemsContext.Provider value={{ items, setItems }}>
          <Routes>
            <Route path='/' element={<ItemsList />} />
            <Route path='/add' element={<AddItem />} />
            <Route path='/edit/:id' element={<EditItem />} />
          </Routes>
        </ItemsContext.Provider>
      </main>
    </BrowserRouter>
  );
}

export default App;
