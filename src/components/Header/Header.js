import React from 'react';
import { NavLink } from 'react-router-dom';
import './Header.sass';

const Header = () => {
  return (
    <header className='site-header'>
      <h1><NavLink to="/" className={({isActive}) => (isActive ? "active" : '')}>React CRUD</NavLink></h1>
      <NavLink to="/add" className="btn btn-primary add-btn">Add Item</NavLink>
    </header>
  );
}

export default Header;