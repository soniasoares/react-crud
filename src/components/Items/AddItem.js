import React, { useContext } from 'react';
import ItemsContext from '../../context/ItemsContext';
import ItemForm from './ItemForm';
import { useNavigate } from "react-router-dom";

const AddItem = () => {
  const {items, setItems} = useContext(ItemsContext);
  const navigate = useNavigate();

  const handleOnSubmit = (item) => {
    setItems([item, ...items]);
    navigate('/');
  }

  return (
    <React.Fragment>
      <ItemForm handleOnSubmit={handleOnSubmit} />
    </React.Fragment>
  );
}

export default AddItem;