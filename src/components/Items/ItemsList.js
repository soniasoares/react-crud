import React, { useContext } from 'react';
import ItemsContext from '../../context/ItemsContext';
import moment from 'moment';
import { useNavigate } from "react-router-dom";
import './ItemsList.sass';

const ItemsList = () => {
  const { items, setItems } = useContext(ItemsContext);
  const navigate = useNavigate();

  const handleRemoveBook = (id) => {
    setItems(items.filter((item) => item.id !== id));
  };

  return (
    <React.Fragment>
      <div className='items-list'>
        {items.map((item,index)=> (
          <article key={index}>
            <h3>
              {item.title}
              {item.subtitle && <span className='subtitle'>{item.subtitle}</span>} 
            </h3>
            <div className='date'>{moment(item.date).format('ll')}</div>
            <div className='description'>{item.description}</div>
            <div className='actions'>
              <button className='btn' onClick={() => navigate(`/edit/${item.id}`)}>Edit</button>
              <button className='btn btn-alert' onClick={() => handleRemoveBook(item.id)}>Delete</button>
            </div>
          </article>
        ))}
      </div>
    </React.Fragment>
  );
}

export default ItemsList;