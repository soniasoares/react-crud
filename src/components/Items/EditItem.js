import React, { useContext } from 'react';
import ItemsContext from '../../context/ItemsContext';
import ItemForm from './ItemForm';
import { useParams, useNavigate } from 'react-router-dom';

const EditItem = () => {
  const { items, setItems } = useContext(ItemsContext);
  const { id } = useParams();
  const itemToEdit = items.find((item) => item.id === id);
  const navigate = useNavigate();

  const handleOnSubmit = (item) => {
    setItems([...items].map(x => x.id === id ? item : x));
    navigate('/');
  };

  return (
    <div>
      <ItemForm item={itemToEdit} handleOnSubmit={handleOnSubmit} />
    </div>
  );
}

export default EditItem;