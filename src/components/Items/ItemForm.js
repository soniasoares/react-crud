import React, { useState } from 'react';
import { v4 as uuidv4 } from 'uuid';
import { NavLink } from 'react-router-dom';

const ItemForm = (props) => {
  const [item, setItem] = useState(() => {
    // the code for setting state will not be executed on every re-render ,
    // only once when the component is mounted
    return {
      title: props.item ? props.item.title : '',
      subtitle: props.item ? props.item.subtitle : '',
      description: props.item ? props.item.description : '',
      date: props.book ? props.book.date : ''
    };
  });
  const [errorMsg, setErrorMsg] = useState('');
  const { title, subtitle, description } = item;

  const handleOnSubmit = (event) => {
    event.preventDefault();    
    const item = {
      id: uuidv4(),
      title,
      subtitle,
      description,
      date: new Date()
    };
    props.handleOnSubmit(item);

  };

  const handleInputChange = (event, required) => {
    const { name, value } = event.target;
    setItem((prevState) => ({
      ...prevState,
      [name]: value
    }));
  };

  return (
    <form onSubmit={handleOnSubmit}>
      <FormField 
        required={true}
        id="itemTitleField" 
        label="Title" 
        type="text" 
        name="title" 
        value={title} 
        onChange={handleInputChange} />
      <FormField 
        required={false}
        id="itemSubtitleField" 
        label="Subtitle" 
        type="text" 
        name="subtitle" 
        value={subtitle} 
        onChange={handleInputChange} />
      <FormField 
        required={true}
        id="itemDescriptionField" 
        label="Description" 
        type="textarea" 
        name="description" 
        value={description} 
        onChange={handleInputChange} />
      <div className='form-submit'>
        <NavLink to="/" className="btn add-btn">Cancel</NavLink>
        <button className='btn btn-primary'>Submit</button>
      </div>
    </form>
  );
}

const FormField = ({required, id, label, type, name, value, onChange}) => {
  return (
    <div className='form-field'>
      <label htmlFor={id}>{label} {required && <span>*</span>}</label>
      {(type === 'textarea') 
        ? <textarea 
          id={id} 
          type={type} 
          name={name}
          value={value}
          onChange={onChange}
          required={required === true}></textarea> 
        : <input 
          id={id} 
          type={type} 
          name={name}
          value={value}
          onChange={onChange}
          required={required === true} />}
    </div>
  );
}

export default ItemForm;